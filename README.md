# tlr-auth-service

Make sure `docker` is installed. To install docker, see the [docs](https://docs.docker.com/engine/install/).

Once installed, clone the repository and run `docker-compose up` in the root of the project.

```bash
docker-compose up
```

This will start the docker containers, and eventually expose the `Keycloak` server at `localhost:8080`. By default, the Keycloak server is only an authentication server, it does not host any user UI yet, until a client is created. 

For convenience a client will be created on startup, called `tlr-default-client` in the `tlr-realm`. A client is needed to give users a UI where they can fill-in there username and password. 

## See the action
To make sure an user exists first, go to: http://localhost:8080/admin/master/console/#/tlr-realm/users. Create a user and set a password on the credentials page. 

Now to mimic an end-to-end flow, we consider the following app as the [`tlr map`](https://www.keycloak.org/app/#url=http://localhost:8080&realm=tlr-realm&client=tlr-default-client). It is configured to act together with the docker containers that run locally. This is pure for testing.

So go to: [https://www.keycloak.org/app/#url=http://localhost:8080&realm=tlr-realm&client=tlr-default-client](https://www.keycloak.org/app/#url=http://localhost:8080&realm=tlr-realm&client=tlr-default-client) and imagine this is the TLR map. A user comes here, and clicks on `login`. When a user does that, it will immediately being redirect to our authentication server, at `localhost:8080` and fill in the user credentials you've created earlier. You will now be redirected back to the origin requester (TLR map), and will have a session now.

## Custom themes
You can install custom UI themes on keycloak, to give your users a seamless user experience that fits the overall design. An easy to integrate UI with React at [keycloakify](https://github.com/keycloakify/keycloakify).